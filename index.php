<?php

    use Phalcon\Mvc\Micro;
    use Phalcon\Mvc\View;

    define('FILEPATH', __DIR__ . '/file_store/'); //folder where files locate
    require('app/ApiMethods.php');

    $app = new Micro();

    $app->before(function() use ($app) {
        ApiMethods::auth();
    });
    
    
    $app->post('/files/{name}', function($name){
        ApiMethods::updateFile($name);
    });
    
    $app->put('/files/{name}', function($name){
        ApiMethods::createFile($name);
    });
    
    $app->delete('/files/{name}', function($name){
        ApiMethods::deleteFile($name);
    });

    $app->get('/files/{name}', function($name) use ($app){
        if (substr($app->request->getURI(), -1) == '/'){
             ApiMethods::readFolder($name); //folder request
        } else {
             ApiMethods::readFile($name); //file request
        }
    });
    
    $app->notFound("ApiMethods::send404");
    $app->handle();
