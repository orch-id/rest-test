# REST TEST#

For run app need installed php framework Phalcon > v.2

### How test? ###

* GET
```
#!bash
curl -i -X GET http://admin:admin@localhost/files/data.txt
curl -i -X GET http://admin:admin@localhost/files/testfolder/
```

* POST
```
#!bash
curl -i -X POST -d "file_content=value1" http://admin:admin@localhost/files/data2.txt
```

* PUT
```
#!bash
curl -i -X PUT -d "file_content=newvalue" http://admin:admin@localhost/files/datanew.txt
```

* DELETE
```
#!bash
curl -i -X DELETE http://admin:admin@localhost/files/datanew.txt
```