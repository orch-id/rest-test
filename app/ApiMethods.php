<?php

use Phalcon\Http\Response;
use Phalcon\Http\Request;

/**
 * METHODS PACK FOR REST TEST SERVER
 *
 */
class ApiMethods
{
    protected static $_path;

    /**
     * void _fsPath()
     * Setup current file/folder path
     * @param string $name file or folder name
     * @return bool|string fs object type or false if not found anything
     */
    protected static function _fsPath($name)
    {
        self::$_path = FILEPATH . $name;
        
        if (file_exists(self::$_path) && !is_dir(self::$_path)){
            $type = 'file';
        } elseif (is_dir(self::$_path)){
            $type = 'dir';
        } else {
            return false;
        }
        
        return $type;
    }


    public static function readFile($fileName)
    {
        if (self::_fsPath($fileName) == 'file'){
            $response = new Response();
            $fileData = file_get_contents(self::$_path);
            $response
                ->setHeader("Content-Type", "text/plain")
                ->sendHeaders();
            $response->setContent($fileData)->send();
        } else {
            self::send404();
        }
    }


    public static function readFolder($folderName)
    {
        if(self::_fsPath($folderName) == 'dir'){
            $response = new Response();
            $response
                ->setHeader("Content-Type", "application/json")
                ->sendHeaders();
            $data = scandir(self::$_path);
            $response->setJsonContent($data)->send();
        } else {
            self::send404();
        }
    }
    
    public static function deleteFile($fileName)
    {
        $response = new Response();
        if(self::_fsPath($fileName) == 'file') {
            if (unlink(self::$_path)){
                $response->setStatusCode(200, "Ok")->sendHeaders();
                die();
            }
        }
        $response->setStatusCode(409, "Conflict")->sendHeaders();
        die('Delete error');
    }
    
    
    public static function createFile($fileName)
    {
        define("FILE_PUT_CONTENTS_ATOMIC_MODE", 0666); 
        $request = new Request();
        $response = new Response();
        
        // secure extention check
        self::_isAllowedExt($fileName);
        
        if(self::_fsPath($fileName) === false) {
            file_put_contents(self::$_path, $request->getPut("file_content"));
            chmod(self::$_path, 0666);
            $response->setStatusCode(200, "Ok")->sendHeaders();
        } else {
            $response->setStatusCode(409, "Conflict")->sendHeaders();
            die('File exist');
        }
    }
    
    
    public static function updateFile($fileName)
    {
        $request = new Request();
        $response = new Response();
        
        if (self::_fsPath($fileName) == 'file'){
            echo 'write to '.self::$_path;
            if(file_put_contents(self::$_path, $request->getPost("file_content"))){
                $response->setStatusCode(200, "Ok")->sendHeaders();
            } else {
                $response->setStatusCode(409, "Conflict")->sendHeaders();
                echo 'Error write to file ';
            }
        } else {
            self::send404();
        }
    }
    
    
    public static function send404()
    {
        $response = new Response();
        $response->setStatusCode(404, "Not Found")->sendHeaders();
        $response->setContent('Not found')->send();
    }


    public static function auth()
    {
        $request = new Request();
        if(is_null($request->getBasicAuth())  /*|| ��� �������� ������ ������ */){
            header('WWW-Authenticate: Basic realm="TEST REST API"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'need auth';
            die();
        }
    }
    
    protected function _isAllowedExt($fileName)
    {
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $allowed =  array('txt','log','ini');
        if(!in_array($ext,$allowed) ) {
            $response->setStatusCode(409, "Conflict")->sendHeaders();
            die('disallowed extention');
        }
    }

}